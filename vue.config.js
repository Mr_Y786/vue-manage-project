// module.exports = {
//     devServer: {
//       proxy: {
//         '/api': {
//           target: 'http://admin.raz-kid.cn', //对应自己的接口
//           changeOrigin: true,
//           ws: true,
//           pathRewrite: {
//             '^/api': '/'
//           }
//         }
//       }
//     }
//   }

module.exports = {
  //webpack - dev - server 的配置项
  devServer: {
    open: true,
    host: "localhost",
    port: 8080,
    https: false,
    disableHostCheck: true,
    proxy: {
      "/api": {
        target: "http://admin.raz-kid.cn/api/", //对应自己的接口
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          "^/api": "",
        },
      },
    },
  },
  outputDir: undefined,
  assetsDir: "static",
  runtimeCompiler: undefined,
  productionSourceMap: true,
  parallel: undefined,
  css: undefined,
};