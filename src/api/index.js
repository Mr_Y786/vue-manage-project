import { http } from "./http"
import qs from "qs"
//登陆
export function login(data) {
     return http("manage/user/login.do", "POST", qs.stringify(data)).then(res => {
          return res;

     })
}
//退出登陆
export function loginOut() {
     return http("user/logout.do", "POST").then(res => {
          return res;

     })
}
//首页数量
export function indexNum() {
     return http("manage/statistic/base_count.do", "GET").then(res => {
          return res.data;
     })
}
//商品列表
export function shopList(obj) {
     return http("manage/product/list.do", "GET", {}, obj).then(res => {
          return res.data;
     })
}
//商品上架状态
export function setStatus(obj) {
     return http("manage/product/set_sale_status.do", "POST", qs.stringify(obj)).then(res => {
          return res;
     })
}

//商品查询
export function searchShop(obj) {
     return http("manage/product/search.do", "GET", {}, obj).then(res => {
          return res.data;
     })
}

//商品详情
export function getInfo(id) {
     return http("manage/product/detail.do", "GET", {}, { productId: id }).then(res => {
          return res.data;
     })
}
//商品分类
export function category(id) {
     return http("manage/category/get_category.do", "GET", {}, { categoryId: id}).then(res => {
          return res.data;
     })
}
//新增商品
export function addShop(obj) {
     return http("manage/product/save.do", "POST", qs.stringify(obj)).then(res => {
          return res.data;
     })
}
