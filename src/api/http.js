import axios from "axios";
import { Message } from 'element-ui';
import router from "../router"
const instance = axios.create({
    // baseURL: "http://admin.raz-kid.cn"
})

export function http(url, method, data, params) {
    
    return new Promise((resolve, reject) => {
        instance({
            url:"/api/"+url,
            method,
            data,
            params
        }).then(res => {
            if((res.status >= 200 && res.status < 300) || res.status === 304){
                if(res.data.status==0){
                    resolve(res.data)
                }else if(res.data.status==10){
                    Message({
                        showClose: true,
                        message: res.data.msg,
                        type: 'error'
                    });
                    router.push("/login")
                }else{
                    Message({
                        showClose: true,
                        message: res.statusText,
                        type: 'error'
                    });
                    reject(res)
                }
            }
            resolve(res)
        }).catch(res => {
            //失败
            reject(res)
        })

    })
}