import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: () => import(/* webpackChunkName: "login" */ '../components/login.vue')
  },
  {
    path: '/home',
    component: () => import(/* webpackChunkName: "home" */ '../components/home/home.vue'),
    children: [
      {
        path: '/',
        redirect: 'index'
      },
      {
        path: 'index',
        component: () => import(/* webpackChunkName: "home" */ '../components/home/homeIndex.vue'),
      },
      {
        path: 'shop',
        component: () => import(/* webpackChunkName: "shop" */ '../components/shop/shop.vue'),
      },
      {
        path: 'addShop',
        component: () => import(/* webpackChunkName: "addShop" */ '../components/shop/addShop.vue'),
      },
      {
        path: 'editShop/:id',
        component: () => import(/* webpackChunkName: "editShop" */ '../components/shop/editShop.vue'),
      },
      {
        path: 'shopInfo/:id',
        component: () => import(/* webpackChunkName: "shopInfo" */ '../components/shop/shopInfo.vue'),
      },
      {
        path: 'classify',
        component: () => import(/* webpackChunkName: "classify" */ '../components/classify/classify.vue'),
      },
      {
        path: 'subClassify/:id',
        component: () => import(/* webpackChunkName: "subClassify" */ '../components/classify/subClassify.vue'),
      },
      {
        path: 'order',
        component: () => import(/* webpackChunkName: "order" */ '../components/order/order.vue'),
      },
      {
        path: 'orderInfo/:id',
        component: () => import(/* webpackChunkName: "orderInfo" */ '../components/order/orderInfo.vue'),
      },
      {
        path: 'user',
        component: () => import(/* webpackChunkName: "user" */ '../components/user/user.vue'),
      },
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
